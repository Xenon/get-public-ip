PUBIP

A one liner script for obtaining your public ip.

Backstory :
Well. Since i tend to route all my traffic either to a vpn, proxy, or tor.
I love to see my public ip after i got my complete connection running.
So i just made a really simple 1 line script to execute, either automatically at when you start the terminal, or by using the "pubip" command.

Instruction :

1. Clone the repo obviously

Use Case :
A.If you just want to use it just by typing "pubip" command

1. Move the pubip file to your /bin folder
2. Change the file permission using chmod "chmod 777 pubip"
3. Enjoy yourself watching your public ip on terminal like a hax0rr

B. If you want to use it the regular way.
1. Just move the "pubip" files to your "home" folder
2. Execute by using "sh ~/pubip

C. If you want to make your ip to be shown at the start of the terminal
1. Do the use case one
2. Edit your .bashrc files and add newline containing word "pubip"
